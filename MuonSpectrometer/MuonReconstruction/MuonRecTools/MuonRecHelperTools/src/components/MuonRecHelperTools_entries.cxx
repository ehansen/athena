#include "MuonRecHelperTools/MuonEDMHelperTool.h"
#include "MuonRecHelperTools/MuonEDMPrinterTool.h"
#include "../MuonSegmentConverterTool.h"

using namespace Muon;

DECLARE_COMPONENT( MuonEDMHelperTool )
DECLARE_COMPONENT( MuonEDMPrinterTool )
DECLARE_COMPONENT( MuonSegmentConverterTool )

